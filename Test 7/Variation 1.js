

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function () {
    jQuery('body').addClass('test7v1');

    function getScript(src, callback) {
        var s = document.createElement('script');
        s.src = src;
        s.async = true;
        s.onreadystatechange = s.onload = function() {
    
                callback();
    
        };
        document.querySelector('head').appendChild(s);
    };

    var content = {
        afterpay_image : '//cdn.optimizely.com/img/6092490016/3867c669ba764edb95dd66da854b408f.png',
        slider : [
            {
                image   : 'https://placehold.it/320x218',
                caption : "Shop d'lites"
            },
            {
                image   : 'https://placehold.it/320x218',
                caption : "Shop something else"
            }
        ],
        sneakers : [
            {
                image   : 'https://www.skechers.com.au/media/catalog/product/cache/f073062f50e48eb0f0998593e568d857/1/1/11918.bwgd_11918_bwgd_large_127198.jpg',
                title   : "Women's D'Lites - Runway Ready",
                price   : '$119.99',
                link    : 'https://www.skechers.com.au/women-s-d-lites-runway-ready.html'
            },
            {
                image   : 'https://www.skechers.com.au/media/catalog/product/cache/f073062f50e48eb0f0998593e568d857/9/9/99999693.wgpk_99999693_wgpk_large_127391.jpg',
                title   : "Women's D'Lites - Sweet Monster",
                price   : '$119.99',
                link    : 'https://www.skechers.com.au/women-s-d-lites-sweet-monster.html'
            },
            {
                image   : 'https://www.skechers.com.au/media/catalog/product/cache/f073062f50e48eb0f0998593e568d857/1/1/11977.ltpk_11977_ltpk_large_127218.jpg',
                title   : "Women's D'Lites - Bright Blossoms",
                price   : '$119.99',
                link    : 'https://www.skechers.com.au/women-s-d-lites-bright-blossoms-1.html'
            },
            {
                image   : 'https://www.skechers.com.au/media/catalog/product/cache/f073062f50e48eb0f0998593e568d857/9/9/99999693.wgy_99999693_wgy_large_127399.jpg',
                title   : "Women's D'Lites - Sweet Monster",
                price   : '$119.99',
                link    : 'https://www.skechers.com.au/women-s-d-lites-sweet-monster-1.html'
            }
        ],
        ctas : [
            {
                image   : '//cdn.optimizely.com/img/6092490016/4596ed58c63f4a35a24f5f92c1c62eff.png',
                caption : "Shop new arrivals",
                link    : 'https://www.skechers.com.au/shop/men/new-arrivals'
            },
            {
                image   : '//cdn.optimizely.com/img/6092490016/0029713b45ca40bc8324767a9045642e.png',
                caption : "Shop best sellers",
                link    : 'https://www.skechers.com.au/shop/women/best-sellers/'
            }
        ]
    }

    getScript('https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/jquery.flexslider.min.js',init_test);

    function init_test(){

        jQuery("#maincontent .background-image, #maincontent .home-title, #maincontent .home-categories, #maincontent .homepage-content").remove();

        jQuery('<div class="homepage-content" />').insertAfter('#maincontent .page.messages');

        //slider
        jQuery(".homepage-content").append('<div class="flexslider"><ul class="slides" /></div>');

        content.slider.forEach(function(slide){
            jQuery(".homepage-content .flexslider .slides").append('<li><img src="'+slide.image+'" /><span class="caption">'+slide.caption+'</span></li>');
        })

        jQuery(".homepage-content").append('<div class="categories"><a href="https://www.skechers.com.au/womens/all-womens.html/">Womens</a><a href="https://www.skechers.com.au/mens/all-mens.html/">Mens</a><a href="https://www.skechers.com.au/kids/all.html/">Kids</a><a href="https://www.skechers.com.au/sale/all-sale.html/">SALE</a></div>')

        jQuery(".homepage-content").append('<div class="afterpay-logo"><img src="'+content.afterpay_image+'" /></div>');

        jQuery(".homepage-content").append('<div class="sneaker-slider-wrap"><h2><span>SNEAKER STYLING</span></h2><div class="sneaker-slider-inner"><div class="sneaker-slider" /></div></div>');

        content.sneakers.forEach(function(sneaker){
            jQuery(".homepage-content .sneaker-slider").append('<a href="'+sneaker.link+'"><img src="'+sneaker.image+'" /><span class="title">'+sneaker.title+'</span><span class="price">'+sneaker.price+'</span></a>');
        });

        jQuery(".flexslider").flexslider({
            directionNav: false,
            controlNav: true
        });

        jQuery(".sneaker-slider").width( jQuery(".sneaker-slider a").length * 190 - 30 - 20);

        jQuery(".homepage-content").append('<div class="image-ctas" />');

        content.ctas.forEach(function(cta){
            jQuery(".image-ctas").append('<a href="'+cta.link+'" class="image-cta"><img src="'+cta.image+'" /><span class="caption-wrap"><span class="caption">'+cta.caption+'</span></span></a>');
        });

        jQuery(".homepage-content").append('<footer class="page-footer js-page-footer"><div class="footer content"> <a class="social-link" href="https://www.instagram.com/skechersaustralia/"><svg xmlns="http://www.w3.org/2000/svg" class="svg-icon instagram"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-instagram"></use> </svg> Follow us on instagram</a> <a class="social-link" href="https://www.facebook.com/Skechersaustralia/"><svg xmlns="http://www.w3.org/2000/svg" class="svg-icon facebook"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-facebook"></use> </svg> Like us on facebook</a> <div class="footer-block columns"><div class="footer-newsletter small-12"><div class="block newsletter"> <h5 class="title">Sign up for your chance to win a $1000 shopping spree!</h5> <div class="content"> <form class="form subscribe" novalidate="novalidate" action="https://www.skechers.com.au/newsletter/subscriber/new/" method="post" id="newsletter-validate-detail"> <div class="field newsletter"> <div class="control"> <input name="email" type="email" id="newsletter" class="input" placeholder="Enter your email address" data-validate="{required:true, "validate-email":true}"> </div></div><div class="actions"> <button class="action subscribe inverted" title="Sign up" type="submit"> Sign up </button> </div></form> </div></div></div></div></div><div class="paysecure small-12"><div class="payment"><div class="icons"><img class="payments" src="https://www.skechers.com.au/static/version1539938968/frontend/Ewave/skechers-mobile/en_AU/images/secure-pay-logos-with-afterpay.png" alt="payments" data-pagespeed-url-hash="2965739869" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></div></div></div><small class="copyright"> <span>Â© 2018 Skechers Australia | All rights reserved</span></small></div></div><div class="language-block"><a class="language link" id="language-link" data-test="language-link"> <svg xmlns="http://www.w3.org/2000/svg" class="svg-icon world"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-world"></use> </svg> <span class="base">Australia</span></a></div><div class="footer-snapchat helper_hidden"></div></footer>')

    }
    jQuery('head').append('<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/flexslider.min.css" />');
},'#maincontent');

jQuery('head').append(`<style>
.test7v1 #maincontent {max-width:none; padding-bottom:0;}
.test7v1 .homepage-content .flexslider {border:none; border-radius:0; box-shadow:none; margin:0;}
.test7v1 .homepage-content .flexslider .caption {display:block; position:absolute; bottom:40px; right:20px; height:40px; line-height:42px; text-transform:uppercase; background:#fff; font-size:15px; font-weight:bold; color:#0064BA; padding:0 15px; }
.test7v1 .homepage-content .flexslider .flex-control-nav {bottom:17px; font-size:0; z-index:100; }
.test7v1 .homepage-content .flexslider .flex-control-nav a {border:1px solid #fff; border-radius:100%; background:transparent; width:8px; height:8px; box-shadow:none;}
.test7v1 .homepage-content .flexslider .flex-control-nav a.flex-active {background:#fff;}

.test7v1 .homepage-content .categories {padding:0 10px; margin:10px 0 26px 0;}
.test7v1 .homepage-content .categories:after {content:""; display:block; clear:both;}
.test7v1 .homepage-content .categories a {font-family: 'BerninoSans','Helvetica Neue',Helvetica,Arial,sans-serif; font-size:17px; letter-spacing:-0.2px; color:#0064BA; text-transform:uppercase; text-align:center; line-height:33px; padding:10px 0; display:block; width:50%; float:left;}
.test7v1 .homepage-content .categories a:nth-child(1), .test7v1 .homepage-content .categories a:nth-child(2) {border-bottom:1px solid #D8D8D8;}
.test7v1 .homepage-content .categories a:nth-child(1), .test7v1 .homepage-content .categories a:nth-child(3) {border-right:1px solid #D8D8D8;}

.test7v1 .afterpay-logo {margin: 0 10px; background:#55B7BA; }
.test7v1 .afterpay-logo img {margin:0 auto; display:block;}


.test7v1 .sneaker-slider-wrap h2 {position:relative; text-align:center; margin: 16px 10px;}
.test7v1 .sneaker-slider-wrap h2:after {content:""; display:block; position:absolute; left:0; top:50%; margin-top:-1px; width:100%; height:2px; background:#484647}
.test7v1 .sneaker-slider-wrap h2 span {display:inline-block; background:#fff; font-size:23px; color:#484647; padding:0 10px; position:relative; z-index:10; font-family:'BerninoSans','Helvetica Neue',Helvetica,Arial,sans-serif;}

.test7v1 .sneaker-slider-wrap .sneaker-slider-inner {width:100%; overflow-x: scroll;}
.test7v1 .sneaker-slider-wrap .sneaker-slider:after {content:""; display:block; clear:both; padding-bottom:20px;}
.test7v1 .sneaker-slider-wrap .sneaker-slider a {display:block; width: 150px; margin-right:30px; float:left; margin:0 10px; }
.test7v1 .sneaker-slider-wrap .sneaker-slider a:last {margin-right:0;}
.test7v1 .sneaker-slider-wrap .sneaker-slider a img {display:block; max-width:129px; height:auto; margin:0 auto;}
.test7v1 .sneaker-slider-wrap .sneaker-slider a span.title {font-size:12px; line-height:13px; color:#484647; font-weight:700; display:block; max-width:110px; text-align:center; margin:0 auto;}
.test7v1 .sneaker-slider-wrap .sneaker-slider a span.price {font-size:12px; line-height:13px; color:#484647; font-weight:300; padding-top:7px; text-align:center; display:block;}

.test7v1 .image-ctas {margin:0 10px;}
.test7v1 .image-ctas .image-cta {position:relative; display:block; margin-bottom:15px;}
.test7v1 .image-ctas .image-cta img {display:block; width:100%; height:auto;}
.test7v1 .image-ctas .image-cta span.caption-wrap {position:absolute; bottom:11px; z-index:100; display:block; width:100%; text-align:center;}
.test7v1 .image-ctas .image-cta span.caption {display:inline-block;  height:40px; line-height:42px; text-transform:uppercase; background:#fff; font-size:15px; font-weight:bold; color:#0064BA; padding:0 15px; }

.test7v1 .footer.content .block {float:none; margin-top:20px}
.test7v1 .footer.content .block h5.title {font-size: 22px; line-height: 26px;}
.test7v1 .page-footer img.payments {display:block; margin:0 auto;}
.test7v1 .page-footer small.copyright {display:block; text-align:center; margin:15px 0;}

.test7v1 .page-footer .social-link { position:relative; margin-bottom:10px; display: block; height: 50px; background: #555555; color: #fff; line-height: 50px; text-align: left; padding-left: 60px; font-size: 15px; text-transform: uppercase; font-weight: 700;}
.test7v1 .page-footer .social-link svg {position:absolute; left:12px; top:9px;}
</style>`);
