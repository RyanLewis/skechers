console.clear();
$ = jQuery.noConflict();
$('body.catalog-product-view').addClass('opt2-v1');


/**************************************
    LAYOUT V1
**************************************/ 
$(".back-to-top").click(function(e){
    e.preventDefault();
    e.stopPropagation();
    $('html, body').animate({
        scrollTop: 0
    }, 500);
});




/**************************************
    STYLES
**************************************/
$('head').append(`
<style type="text/css">
    .opt2-v1 .product.toolbar {display:none;}
    .opt2-v1 #launcher {display:none !important;}
    /*.opt2-v1 .ze-widget-container {height:auto !important;}*/
    
    .opt2-v1 .modal-popup.product-info-popup {position:relative !important; left:auto !important; right:auto !important; top:auto !important; bottom:auto !important; width: auto !important; height:auto !important; overflow:visible !important; max-height:none !important; padding:10px !important; background-color:#e2e2e2; z-index:1 !important;}
    .opt2-v1.body._has-modal {position:relative !important; left:auto !important; right:auto !important; top:auto !important; bottom:auto !important; width: auto !important; height:auto !important; overflow:visible !important; max-height:none !important;}
    .opt2-v1 .modal-popup.product-info-popup .modal-inner-wrap {position:relative !important; left:auto !important; right:auto !important; top:auto !important; bottom:auto !important; width: auto !important; height:auto !important; overflow:visible !important; max-height:none !important; transform:none !important; display:block !important;}
    .opt2-v1 .modal-popup.product-info-popup .modal-inner-wrap .modal-content {visibility:visible;}
    
    .opt2-v1 .modal-popup.product-info-popup #product-info {background-color:#ffffff;}
    
    
    .opt2-v1 .custom-gallery .fotorama__arr--prev {display:block !important; left:20px !important; width:13px !important; height:20px !important; background:transparent url("https://i.imgur.com/cWeboec.png") no-repeat left top !important; background-size:13px auto !important; top:auto !important; bottom:12px !important;}
    .opt2-v1 .custom-gallery .fotorama__arr--next {display:block !important; right:20px !important; width:13px !important; height:20px !important; background:transparent url("https://i.imgur.com/naBfZQj.png") no-repeat left top !important; background-size:13px auto !important; top:auto !important; bottom:12px !important;}
    
    .opt2-v1 .custom-gallery .fotorama__arr__arr {display:none !important;}
</style>
`);