console.clear();
$ = jQuery.noConflict();
$('body.catalog-product-view').addClass('opt2-v2');


/**************************************
    LAYOUT V2
**************************************/ 
$(".opt2-v2 #product-info-action").addClass('custom-button').text('Features');



/**************************************
    STYLES
**************************************/
$('head').append(`
<style type="text/css">
    .opt2-v2 .product.toolbar .comments {display:none;}
    .opt2-v2 .product.toolbar .social {display:none;}
    .opt2-v2 .product.toolbar #product-info-action.custom-button {color:#ffffff; font-size:13px; background-color:#5e5e5e; border:1px solid #828282; width:86px; height:38px; line-height:36px; padding:0; margin:0; -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;}
</style>
`);